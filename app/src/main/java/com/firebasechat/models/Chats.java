package com.firebasechat.models;


import java.util.HashMap;
import java.util.Map;

public class Chats {

    private String receiverId;
    private String chatId;

    public Chats(String receiverId, String chatId) {
        this.receiverId = receiverId;
        this.chatId = chatId;
    }

    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put(receiverId, chatId);
        return map;
    }
}