package com.firebasechat.models;


public class UserDetails {

    private String chatId;
    private User user;

    public UserDetails() {
    }

    public UserDetails(String chatId, User user) {
        this.chatId = chatId;
        this.user = user;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}