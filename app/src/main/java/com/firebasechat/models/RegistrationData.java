package com.firebasechat.models;


import android.net.Uri;

public class RegistrationData {
    private String name;
    private String email;
    private String password;
    private String repeatPassword;
    private Uri photoUri;

    public RegistrationData(String name, String email, String password, String repeatePassword, Uri photoUri) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.photoUri = photoUri;
        this.repeatPassword = repeatePassword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Uri getPhotoUri() {
        return photoUri;
    }

    public void setPhotoUri(Uri photoUri) {
        this.photoUri = photoUri;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }
}