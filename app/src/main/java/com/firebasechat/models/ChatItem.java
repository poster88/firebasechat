package com.firebasechat.models;


public class ChatItem {
    private String imageUrl;
    private int viewType;
    private Message message;

    public ChatItem() {
    }

    public ChatItem(String imageUrl, int viewType, Message message) {
        this.imageUrl = imageUrl;
        this.viewType = viewType;
        this.message = message;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChatItem item = (ChatItem) o;

        if (viewType != item.viewType) return false;
        if (imageUrl != null ? !imageUrl.equals(item.imageUrl) : item.imageUrl != null)
            return false;
        return message != null ? message.equals(item.message) : item.message == null;
    }

    @Override
    public int hashCode() {
        int result = imageUrl != null ? imageUrl.hashCode() : 0;
        result = 31 * result + viewType;
        result = 31 * result + (message != null ? message.hashCode() : 0);
        return result;
    }
}