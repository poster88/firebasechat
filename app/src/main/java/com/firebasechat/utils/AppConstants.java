package com.firebasechat.utils;


public final class AppConstants {

    public static final String TIMESTAMP_FORMAT = "HH:mm";
    public static final int PASS_MIN_LENGTH = 6;

    public static final String ERROR_EMAIL_IS_ALREADY_IN_USE = "The email address is already in use by another account.";
    public static final String ERROR_EMAIL_IS_BADLY_FORMATTED = "The email address is badly formatted.";
    public static final String ERROR_DEFAULT_TEXT = "Oops, some is wrong!";
    public static final String ERROR_NO_USER_IDENTIFIER = "There is no user record corresponding to this identifier.";
    public static final String ERROR_INVALID_PASSWORD = "The password is invalid or the user does not have a password.";

    public static final int REQUEST_READ_PERMISSION = 9003;
    public static final int PHOTO_REQUEST = 9002;
    public static final String PHOTO_TYPE = "image/*";

    public static final String PATH_USERS_IMAGES = "users_images";
    public static final String PATH_USERS = "users";
    public static final String PATH_CHATS = "chats";
    public static final String PATH_MESSAGES = "messages";
    public static final String KEY_NAME = "name";
    public static final String KEY_USER_DETAILS_LIST = "userDetailsList";
}