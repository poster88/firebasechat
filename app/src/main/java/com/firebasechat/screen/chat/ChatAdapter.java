package com.firebasechat.screen.chat;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebasechat.R;
import com.firebasechat.screen.BaseAdapter;
import com.firebasechat.models.ChatItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChatAdapter extends BaseAdapter {

    public static final int VIEW_TYPE_SENDER = 0;
    public static final int VIEW_TYPE_RECEIVER = 1;

    private List<ChatItem> chatItems = new ArrayList<>();

    public ChatAdapter(List<ChatItem> chatItems) {
        this.chatItems = chatItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_SENDER) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_chat_sender, parent, false);
            return new ChatHolderTypeSender(view);
        } else {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_chat_receiver, parent, false);
            return new ChatHolderTypeReceiver(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ChatItem item = chatItems.get(position);
        if (holder.getItemViewType() == VIEW_TYPE_SENDER) {
            ChatHolderTypeSender h = (ChatHolderTypeSender) holder;
            h.txtMessage.setText(item.getMessage().getMessageText());
            h.txtMessageTime.setText(super.formatTime(item.getMessage().getTimestamp()));
        } else {
            ChatHolderTypeReceiver h = (ChatHolderTypeReceiver) holder;
            h.txtMessage.setText(item.getMessage().getMessageText());
            h.txtMessageTime.setText(super.formatTime(item.getMessage().getTimestamp()));
            super.loadImage(h.imgAvatar, item.getImageUrl());
        }
    }

    @Override
    public int getItemCount() {
        return chatItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return chatItems.get(position)
                .getViewType();
    }

    public static class ChatHolderTypeSender extends RecyclerView.ViewHolder {
        @BindView(R.id.item_chat_txt_time) TextView txtMessageTime;
        @BindView(R.id.item_chat_txt_message) TextView txtMessage;

        public ChatHolderTypeSender(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static class ChatHolderTypeReceiver extends RecyclerView.ViewHolder {
        @BindView(R.id.item_chat_img_user) CircleImageView imgAvatar;
        @BindView(R.id.item_chat_txt_time) TextView txtMessageTime;
        @BindView(R.id.item_chat_txt_message) TextView txtMessage;

        public ChatHolderTypeReceiver(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}