package com.firebasechat.screen.chat;


import com.firebasechat.models.ChatItem;
import com.firebasechat.models.User;

import java.util.List;

public interface ChatView {

    void showErrorMessage(String error);

    void initToolbar(User user);

    void closeScreen();

    void showProgress();

    void hideProgress();

    void showChat(List<ChatItem> items);

    void showMessage(String message);

    void updateChat(int position);

    void removeText();
}