package com.firebasechat.screen.chat;


import com.firebasechat.repository.FirebaseChatRepository;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.firebasechat.utils.AppConstants.ERROR_DEFAULT_TEXT;


public class ChatPresenterImpl implements ChatPresenter {

    public static final String TAG = ChatPresenterImpl.class.getSimpleName();

    private ChatView view;
    private FirebaseChatRepository repository;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public ChatPresenterImpl(FirebaseChatRepository repository, ChatView view) {
        this.repository = repository;
        this.view = view;
    }

    @Override
    public void init() {
        compositeDisposable.add(initChat());
    }

    private Disposable initChat() {
        return repository.initChat()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    if (view != null) {
                        view.initToolbar(repository.getReceiverData());
                        view.showProgress();
                    }
                })
                .doAfterTerminate(() -> {
                    if (view != null) view.hideProgress();
                })
                .doOnComplete(() -> compositeDisposable.add(startChatEventListener()))
                .subscribe(chatItems -> {
                            if (view != null) view.showChat(chatItems);
                        },
                        throwable -> {
                            if (view != null) view.showErrorMessage(ERROR_DEFAULT_TEXT);
                        });
    }

    private Disposable startChatEventListener() {
        return repository.addChatEventListener()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(position -> {
                    if (view != null) view.updateChat(position);
                });
    }

    @Override
    public void onNavigationBtnClick() {
        if (view != null) view.closeScreen();
    }

    @Override
    public void onMenuItemClick(int itemId) {
        if (view != null) view.showMessage("will be soon:)");
    }

    @Override
    public void onSendMessageBtnClick(String message) {
        if (!message.isEmpty()) {
            repository.sendMessage(message);
            if (view != null) view.removeText();
        }
    }

    @Override
    public void onDestroy() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
        repository.removeChatEventListener();
        repository.clearData();
    }
}