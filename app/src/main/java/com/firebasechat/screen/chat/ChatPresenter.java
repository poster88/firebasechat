package com.firebasechat.screen.chat;


public interface ChatPresenter {

    void init();

    void onNavigationBtnClick();

    void onMenuItemClick(int itemId);

    void onSendMessageBtnClick(String message);

    void onDestroy();
}