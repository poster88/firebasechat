package com.firebasechat.screen.chat;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.firebasechat.screen.BaseActivity;
import com.firebasechat.R;
import com.firebasechat.app.App;
import com.firebasechat.models.ChatItem;
import com.firebasechat.models.User;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.VISIBLE;

public class ChatActivity extends BaseActivity implements ChatView {

    public static final String TAG = ChatActivity.class.getSimpleName();

    @BindView(R.id.activity_chat_toolbar) Toolbar toolbar;
    @BindView(R.id.activity_chat_et_message) EditText etMessage;
    @BindView(R.id.activity_chat_btn_send_message) ImageButton imgBtnSend;
    @BindView(R.id.activity_chat_rv_messages) RecyclerView rvMessages;
    @BindView(R.id.activity_chat_pb_loading) ProgressBar progressBar;

    private ChatPresenterImpl presenter;
    private ChatAdapter adapter;
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);

        presenter = new ChatPresenterImpl(App.getFirebaseChatRepository(), this);
        presenter.init();
    }

    @Override
    public void showErrorMessage(String error) {
        super.showToast(this, error);
    }

    @Override
    public void initToolbar(User user) {
        toolbar.setTitle(user.getName());
        toolbar.setSubtitle(user.getEmail());
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.inflateMenu(R.menu.menu_chat);
        toolbar.setNavigationOnClickListener(v -> presenter.onNavigationBtnClick());
        toolbar.setOnMenuItemClickListener(item -> {
            presenter.onMenuItemClick(item.getItemId());
            return false;
        });
    }

    @OnClick(R.id.activity_chat_btn_send_message)
    public void onSendMessageBtnClick() {
        presenter.onSendMessageBtnClick(etMessage.getText().toString());
    }

    @Override
    public void closeScreen() {
        finish();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(VISIBLE);
    }

    @Override
    public void removeText() {
        etMessage.getText().clear();
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showChat(List<ChatItem> items) {
        layoutManager = new LinearLayoutManager(this);
        adapter = new ChatAdapter(items);
        rvMessages.setVisibility(VISIBLE);
        rvMessages.setLayoutManager(layoutManager);
        rvMessages.setAdapter(adapter);
        setupScrollPosition(items.size());
    }

    @Override
    public void showMessage(String message) {
        super.showToast(this, message);
    }

    @Override
    public void updateChat(int position) {
        adapter.notifyItemChanged(position);
        setupScrollPosition(position);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    private void setupScrollPosition(int size) {
        if (size > 1) {
            layoutManager.scrollToPosition(size - 1);
        }
    }
}