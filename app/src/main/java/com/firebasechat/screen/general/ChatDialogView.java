package com.firebasechat.screen.general;


public interface ChatDialogView {

    void showProgress();

    void hideProgress();
}