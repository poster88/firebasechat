package com.firebasechat.screen.general;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.TextView;

import com.firebasechat.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class ChatDialogFragment extends DialogFragment {

    public static final String TAG = ChatDialogFragment.class.getSimpleName();

    @BindView(R.id.layout_progress_indeterminate_txt_message) TextView textMessage;

    private Unbinder unbinder;
    private String message;

    public static ChatDialogView view(FragmentManager fm, String message) {
        return new DialogView(fm, message);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = View.inflate(getContext(), R.layout.layout_progress_indeterminate, null);
        unbinder = ButterKnife.bind(this, view);
        textMessage.setText(message);
        return setupDialog(view);
    }

    private Dialog setupDialog(View view) {
        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .create();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void setMessage(String message) {
        this.message = message;
    }


    private static class DialogView implements ChatDialogView {

        private final FragmentManager fm;
        private final String message;
        private ChatDialogFragment dialog;

        DialogView(@NonNull FragmentManager fm, @NonNull String message) {
            this.fm = fm;
            this.message = message;
        }

        @Override
        public void showProgress() {
            dialog = (ChatDialogFragment) fm.findFragmentByTag(TAG);
            if (dialog == null) {
                dialog = new ChatDialogFragment();
                dialog.setCancelable(false);
                dialog.setMessage(message);
                dialog.show(fm, TAG);
            }
        }

        @Override
        public void hideProgress() {
            dialog = (ChatDialogFragment) fm.findFragmentByTag(TAG);
            if (dialog != null) {
                dialog.dismissAllowingStateLoss();
            }
        }
    }
}