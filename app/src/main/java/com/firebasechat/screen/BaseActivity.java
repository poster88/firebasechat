package com.firebasechat.screen;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import static android.view.View.VISIBLE;


public class BaseActivity extends AppCompatActivity {

   protected String formatQuery(String query) {
       return query.trim().toLowerCase();
   }

   protected String formatString(String text) {
       return text.trim();
   }

   protected void showToast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
   }

   protected boolean isVisible(View view) {
       return view.getVisibility() == VISIBLE;
   }

    protected boolean isErrorEnabled(TextInputLayout inputLayout) {
        return inputLayout.isErrorEnabled();
    }
}