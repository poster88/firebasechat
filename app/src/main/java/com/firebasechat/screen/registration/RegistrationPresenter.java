package com.firebasechat.screen.registration;


import com.firebasechat.models.RegistrationData;

public interface RegistrationPresenter {

    void onRegistrationBtnClick(RegistrationData data, boolean isErrorEnabled);

    void onDestroy();

    void onFocusChange(int viewId, boolean b);

    void onNavigationClick();
}