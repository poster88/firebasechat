package com.firebasechat.screen.registration;


import com.firebasechat.R;
import com.firebasechat.models.RegistrationData;
import com.firebasechat.repository.FirebaseChatRepository;
import com.firebasechat.utils.CommonUtils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.firebasechat.utils.AppConstants.ERROR_EMAIL_IS_ALREADY_IN_USE;
import static com.firebasechat.utils.AppConstants.ERROR_EMAIL_IS_BADLY_FORMATTED;
import static com.firebasechat.utils.AppConstants.PASS_MIN_LENGTH;

public class RegistrationPresenterImpl implements RegistrationPresenter{

    public static final String TAG = RegistrationPresenterImpl.class.getSimpleName();

    private FirebaseChatRepository repository;
    private RegistrationView view;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public RegistrationPresenterImpl(FirebaseChatRepository repository, RegistrationView view) {
        this.repository = repository;
        this.view = view;
    }

    @Override
    public void onRegistrationBtnClick(RegistrationData data, boolean isErrorEnabled) {
        if (!isErrorEnabled && validateFields(data)) {
             compositeDisposable.add(registration(data));
        }
    }

    private Disposable registration(RegistrationData data) {
        return repository.registerUser(data)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    if (view != null) view.showProgress();
                })
                .doAfterTerminate(() -> {
                    if (view != null) view.hideProgress();
                })
                .subscribe(() -> {
                    if (view != null) {
                        view.showCompleteMessage();
                        view.showUsersListScreen();
                    }
                }, throwable -> handleError(throwable.getMessage()));
    }

    private void handleError(String error) {
        if (error.equals(ERROR_EMAIL_IS_ALREADY_IN_USE)) {
            view.showEmailErrorMessage(R.string.error_email_is_already_in_use);
        } else if (error.equals(ERROR_EMAIL_IS_BADLY_FORMATTED)) {
            view.showEmailErrorMessage(R.string.error_email_badly_formatted);
        } else {
            view.showErrorMessage(error);
        }
    }

    @Override
    public void onDestroy() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
        view = null;
    }

    @Override
    public void onFocusChange(int viewId, boolean b) {
        int resId = 0;
        if (viewId == R.id.activity_registration_et_name) {
            resId = b ? R.drawable.ic_person_accent : R.drawable.ic_person_primary;
        }
        if (viewId == R.id.activity_registration_et_email) {
            resId = b ? R.drawable.ic_email_accent : R.drawable.ic_email_primary;
        }
        if (viewId == R.id.activity_registration_et_password) {
            resId = b ? R.drawable.ic_key_password_accent : R.drawable.ic_key_password_primary;
        }
        if (viewId == R.id.activity_registration_et_rep_password) {
            resId = b ? R.drawable.ic_key_password_accent : R.drawable.ic_key_password_primary;
        }
        view.changeDrawable(viewId, resId);
    }

    @Override
    public void onNavigationClick() {
        view.closeScreen();
    }

    private boolean validateFields(RegistrationData data) {
        String name = data.getName().trim();
        String email = data.getEmail().trim();
        String password = data.getPassword().trim();
        String repPassword = data.getRepeatPassword().trim();
        return isValidFields(name, email, password, repPassword);
    }

    private boolean isValidFields(String name, String email, String password, String repPassword) {
        boolean isValid = true;
        if (name.isEmpty() && view != null) {
            view.showNameErrorMessage(R.string.error_empty_field);
            isValid = false;
        }
        if (email.isEmpty() && view != null) {
            view.showEmailErrorMessage(R.string.error_empty_field);
            isValid = false;
        }
        if (!CommonUtils.isEmailValid(email) && view != null) {
            view.showEmailErrorMessage(R.string.error_email_badly_formatted);
            isValid = false;
        }
        if (password.isEmpty() && view != null) {
            view.showPasswordErrorMessage(R.string.error_empty_field);
            isValid = false;
        }
        if (repPassword.isEmpty() && view != null) {
            view.showRepPasswordErrMessage(R.string.error_empty_field);
            isValid = false;
        }
        if (!password.isEmpty() && password.length() < PASS_MIN_LENGTH && view != null) {
            view.showPasswordErrorMessage(R.string.error_invalid_length);
            isValid = false;
        }
        if (!repPassword.isEmpty() && repPassword.length() < PASS_MIN_LENGTH && view != null) {
            view.showRepPasswordErrMessage(R.string.error_invalid_length);
            isValid = false;
        }
        if (!password.isEmpty() && password.length() >= PASS_MIN_LENGTH
                && !repPassword.isEmpty() && repPassword.length() >= PASS_MIN_LENGTH
                && !password.equals(repPassword) && view != null) {
            view.showPasswordErrorMessage(R.string.error_do_not_match_passwords);
            view.showRepPasswordErrMessage(R.string.error_do_not_match_passwords);
            isValid = false;
        }
        return isValid;
    }
}