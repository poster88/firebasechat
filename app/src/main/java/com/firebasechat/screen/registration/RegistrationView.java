package com.firebasechat.screen.registration;


public interface RegistrationView {

    void showProgress();

    void hideProgress();

    void showErrorMessage(String error);

    void showCompleteMessage();

    void showUsersListScreen();

    void showEmailErrorMessage(int errorResId);

    void showNameErrorMessage(int errorResId);

    void showPasswordErrorMessage(int errorResId);

    void showRepPasswordErrMessage(int errorResId);

    void closeScreen();

    void changeDrawable(int viewId, int resId);
}