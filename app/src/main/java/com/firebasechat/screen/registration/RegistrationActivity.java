package com.firebasechat.screen.registration;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import com.firebasechat.screen.BaseActivity;
import com.firebasechat.screen.general.ChatDialogFragment;
import com.firebasechat.R;
import com.firebasechat.app.App;
import com.firebasechat.models.RegistrationData;
import com.firebasechat.screen.general.ChatDialogView;
import com.firebasechat.screen.WatchedEditText;
import com.firebasechat.screen.userslist.UsersListActivity;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static com.firebasechat.utils.AppConstants.PHOTO_REQUEST;
import static com.firebasechat.utils.AppConstants.PHOTO_TYPE;
import static com.firebasechat.utils.AppConstants.REQUEST_READ_PERMISSION;

public class RegistrationActivity extends BaseActivity implements RegistrationView,
        View.OnFocusChangeListener {

    public static final String TAG = RegistrationActivity.class.getSimpleName();

    @BindView(R.id.activity_registration_et_name) EditText etName;
    @BindView(R.id.activity_registration_text_il_name) TextInputLayout textInputName;
    @BindView(R.id.activity_registration_et_email) EditText etEmail;
    @BindView(R.id.activity_registration_text_il_email) TextInputLayout textInputEmail;
    @BindView(R.id.activity_registration_et_password) EditText etPassword;
    @BindView(R.id.activity_registration_text_il_password) TextInputLayout textInputPassword;
    @BindView(R.id.activity_registration_et_rep_password) EditText etRepeatPassword;
    @BindView(R.id.activity_registration_text_il_rep_password) TextInputLayout textInputRepPassword;
    @BindView(R.id.activity_registration_img_user) CircleImageView imgUser;
    @BindView(R.id.activity_registration_toolbar) Toolbar toolbar;

    private RegistrationPresenter presenter;
    private ChatDialogView chatDialogView;
    private Uri photoUri;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.registration_label);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(v -> presenter.onNavigationClick());

        chatDialogView = ChatDialogFragment.view(getSupportFragmentManager(),
                getString(R.string.dialog_registration_message));
        presenter = new RegistrationPresenterImpl(App.getFirebaseChatRepository(), this);
        setOnFocusChangeListenerForWidget(this);
        setTextWatcherForFields();
    }

    private void setTextWatcherForFields() {
        new WatchedEditText().startWatchForEditText(etName, textInputName);
        new WatchedEditText().startWatchForEditText(etEmail, textInputEmail);
        new WatchedEditText().startWatchForEditText(etPassword, textInputPassword);
        new WatchedEditText().startWatchForEditText(etRepeatPassword, textInputRepPassword);
    }

    private void setOnFocusChangeListenerForWidget(View.OnFocusChangeListener listener) {
        etName.setOnFocusChangeListener(listener);
        etEmail.setOnFocusChangeListener(listener);
        etPassword.setOnFocusChangeListener(listener);
        etRepeatPassword.setOnFocusChangeListener(listener);
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        presenter.onFocusChange(view.getId(), b);
    }

    @Override
    public void changeDrawable(int viewId, int resId) {
        EditText et = findViewById(viewId);
        int resIdStart = 0;
        int resIdTop = 0;
        int resIdBottom = 0;
        et.setCompoundDrawablesRelativeWithIntrinsicBounds(resIdStart, resIdTop, resId, resIdBottom);
    }

    @OnClick(R.id.activity_registration_img_user)
    public void onAvatarImageClick() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{READ_EXTERNAL_STORAGE}, REQUEST_READ_PERMISSION);
                return;
            }
        }
        openFilePicker();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openFilePicker();
            }
        }
    }

    public void openFilePicker() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(PHOTO_TYPE);
        startActivityForResult(intent, PHOTO_REQUEST);
    }

    public void setImageToView(Uri uri) {
        photoUri = uri;
        Picasso.with(this)
                .load(photoUri)
                .into(imgUser);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PHOTO_REQUEST && resultCode == RESULT_OK) {
            photoUri = data.getData();
            setImageToView(photoUri);
        }
    }

    @OnClick(R.id.activity_registration_btn_reg)
    public void onRegistrationBtnClick() {
        RegistrationData data = new RegistrationData(
                etName.getText().toString(),
                etEmail.getText().toString(),
                etPassword.getText().toString(),
                etRepeatPassword.getText().toString(),
                photoUri);

        presenter.onRegistrationBtnClick(data, isErrorEnabled());
    }

    private boolean isErrorEnabled() {
        return super.isErrorEnabled(textInputName)
                || super.isErrorEnabled(textInputEmail)
                || super.isErrorEnabled(textInputPassword)
                || super.isErrorEnabled(textInputRepPassword);
    }

    @Override
    public void showProgress() {
        chatDialogView.showProgress();
    }

    @Override
    public void hideProgress() {
        chatDialogView.hideProgress();
    }

    @Override
    public void showErrorMessage(String error) {
        super.showToast(this, error);
    }

    @Override
    public void showCompleteMessage() {
        super.showToast(this, getString(R.string.registration_is_succesfull));
    }

    @Override
    public void showUsersListScreen() {
        startActivity(new Intent(this, UsersListActivity.class));
        finish();
    }

    @Override
    public void showEmailErrorMessage(int errorResId) {
        textInputEmail.setError(getString(errorResId));
    }

    @Override
    public void showNameErrorMessage(int errorResId) {
        textInputName.setError(getString(errorResId));
    }

    @Override
    public void showPasswordErrorMessage(int errorResId) {
        textInputPassword.setError(getString(errorResId));
    }

    @Override
    public void showRepPasswordErrMessage(int errorResId) {
        textInputRepPassword.setError(getString(errorResId));
    }

    @Override
    public void closeScreen() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }
}