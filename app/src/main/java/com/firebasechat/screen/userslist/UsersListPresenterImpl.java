package com.firebasechat.screen.userslist;


import com.firebasechat.models.UserDetails;
import com.firebasechat.repository.FirebaseChatRepository;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class UsersListPresenterImpl implements UsersListPresenter {

    public static final String TAG = UsersListPresenterImpl.class.getSimpleName();

    private FirebaseChatRepository repository;
    private UsersListView view;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();


    public UsersListPresenterImpl(FirebaseChatRepository repository, UsersListView view) {
        this.repository = repository;
        this.view = view;
    }

    public void init() {
        compositeDisposable.add(initUserData());
        compositeDisposable.add(getUsersDetails());
        compositeDisposable.add(startListEventListener());
    }

    private Disposable initUserData() {
        return repository.getUserData()
                .subscribeOn(Schedulers.io())
                .subscribe(user -> repository.updateCurrentUserData(user),
                        throwable -> {
                            if (view != null) view.showErrorMessage(throwable.getMessage());
                        });
    }

    private Disposable getUsersDetails() {
        return repository.getUsersDetails()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    if (view != null) view.showProgress();
                })
                .doAfterTerminate(() -> {
                    if (view != null) view.hideProgress();
                })
                .cache()
                .subscribe(usersDetails -> {
                    repository.saveUsersDetailsData(usersDetails);
                    if (view != null) view.showUsers(usersDetails);
                }, throwable -> {
                    if (view != null) view.showErrorMessage(throwable.getMessage());
                });
    }

    private Disposable startListEventListener() {
        return repository.addListEventListener()
                .subscribeOn(Schedulers.io())
                .doOnNext(s -> compositeDisposable.add(updateList()))
                .subscribe();
    }
    private Disposable updateList() {
        return repository.getUsersDetails()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userDetails -> {
                    if (view != null) view.updateAdapter(userDetails);
                });
    }

    @Override
    public void onQueryTextSubmit(String query) {
        compositeDisposable.add(updateAdapter(query));
    }

    private Disposable updateAdapter(String query) {
        return repository.getFilteredUsersDetails(query)
                .subscribe(userDetails -> {
                    if (view != null) view.updateAdapter(userDetails);
                });
    }

    @Override
    public void onBackPressed() {
        repository.signOut();
    }

    @Override
    public void onDestroy() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
        view = null;
        repository.removeUserListEventListener();
    }

    @Override
    public void onItemSelected(UserDetails item) {
        repository.updateReceiverUserData(item);
        if (view != null) view.showChatScreen();
    }
}