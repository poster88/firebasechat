package com.firebasechat.screen.userslist;


import com.firebasechat.models.UserDetails;

public interface UsersListPresenter {

    void init();

    void onQueryTextSubmit(String query);

    void onBackPressed();

    void onDestroy();

    void onItemSelected(UserDetails item);
}