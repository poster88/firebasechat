package com.firebasechat.screen.userslist;


import com.firebasechat.models.UserDetails;

import java.util.List;

public interface UsersListView {

    void showProgress();

    void hideProgress();

    void showUsers(List<UserDetails> data);

    void showErrorMessage(String message);

    void updateAdapter(List<UserDetails> data);

    void showChatScreen();
}