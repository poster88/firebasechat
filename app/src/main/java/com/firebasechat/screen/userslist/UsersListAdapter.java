package com.firebasechat.screen.userslist;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebasechat.R;
import com.firebasechat.screen.RecycleItemClickListener;
import com.firebasechat.models.User;
import com.firebasechat.models.UserDetails;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class UsersListAdapter extends RecyclerView.Adapter<UsersListAdapter.UsersListViewHolder> {

    public static final String TAG = UsersListAdapter.class.getSimpleName();

    private List<UserDetails> data = new ArrayList<>();
    private RecycleItemClickListener itemClickListener;

    public UsersListAdapter(RecycleItemClickListener itemClickListener,List<UserDetails> data) {
        this.itemClickListener = itemClickListener;
        this.data = data;
    }

    @Override
    public UsersListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_user, parent, false);
        return new UsersListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(UsersListViewHolder holder, int position) {
        User user = data.get(position).getUser();
        holder.textName.setText(user.getName());
        holder.textEmail.setText(user.getEmail());
        loadImage(holder.imgAvatar, user.getImageUrl());
        setChatStatusIcon(holder.imgChatStatus, data.get(position).getChatId() != null);
        holder.itemView.setOnClickListener(v ->
                itemClickListener.onItemClickListener(data.get(position)));
    }

    private void setChatStatusIcon(ImageView imageView, boolean isChatEnabled) {
        imageView.setImageResource(isChatEnabled ? R.drawable.ic_chat_accent_24dp
                : R.drawable.ic_chat_gray_24dp);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private void loadImage(final CircleImageView image, final String imageUrl) {
        if (imageUrl == null) {
            image.setImageResource(R.drawable.ic_default_avatar_40);
        } else {
            Picasso.with(image.getContext()).load(imageUrl).into(image);
        }
    }

    public void update(List<UserDetails> userDetails) {
        data = userDetails;
        notifyDataSetChanged();
    }

    public static class UsersListViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_user_img_avatar) CircleImageView imgAvatar;
        @BindView(R.id.item_user_name) TextView textName;
        @BindView(R.id.item_user_email) TextView textEmail;
        @BindView(R.id.item_user_img_chat_status) ImageView imgChatStatus;

        public UsersListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}