package com.firebasechat.screen.userslist;


import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;

import com.firebasechat.screen.BaseActivity;
import com.firebasechat.R;
import com.firebasechat.screen.RecycleItemClickListener;
import com.firebasechat.app.App;
import com.firebasechat.models.UserDetails;
import com.firebasechat.screen.chat.ChatActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class UsersListActivity extends BaseActivity implements UsersListView,
        SearchView.OnQueryTextListener, RecycleItemClickListener {

    public static final String TAG = UsersListActivity.class.getSimpleName();

    @BindView(R.id.activity_users_list_rv_users) RecyclerView rvUsers;
    @BindView(R.id.activity_users_list_pb_loading) ProgressBar pbLoading;
    @BindView(R.id.activity_users_list_toolbar) Toolbar toolbar;

    private UsersListPresenter presenter;
    private UsersListAdapter usersListAdapter;
    private SearchView searchView;
    private LinearLayoutManager layoutManager;
    private DividerItemDecoration itemDecoration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_list);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.users_title);
        setSupportActionBar(toolbar);

        presenter = new UsersListPresenterImpl(App.getFirebaseChatRepository(), this);
        presenter.init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_users_list, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        setSearchView(searchItem);
        return super.onCreateOptionsMenu(menu);
    }

    private void setSearchView(MenuItem searchItem) {
        searchView = (SearchView) searchItem.getActionView();
        searchView.setQueryHint(getString(R.string.title_search));
        searchView.setIconifiedByDefault(false);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        if (searchManager != null) searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
    }

    @Override
    public void showProgress() {
        pbLoading.setVisibility(VISIBLE);
    }

    @Override
    public void hideProgress() {
        pbLoading.setVisibility(GONE);
    }

    @Override
    public void showUsers(List<UserDetails> data) {
        layoutManager = new LinearLayoutManager(this);
        initItemDecoration();
        initUsersList(data);
        searchView.setOnQueryTextListener(this);
    }

    private void initItemDecoration() {
        itemDecoration = new DividerItemDecoration(this, layoutManager.getOrientation());
        itemDecoration.setDrawable(getResources().getDrawable(R.drawable.item_user_divider));
    }

    private void initUsersList(List<UserDetails> data) {
        usersListAdapter = new UsersListAdapter(this, data);
        rvUsers.setVisibility(VISIBLE);
        rvUsers.setLayoutManager(layoutManager);
        rvUsers.setAdapter(usersListAdapter);
        rvUsers.addItemDecoration(itemDecoration);
    }

    @Override
    public void showErrorMessage(String error) {
        super.showToast(this, error);
    }

    @Override
    public void updateAdapter(List<UserDetails> data) {
        usersListAdapter.update(data);
    }

    @Override
    public void showChatScreen() {
        startActivity(new Intent(this, ChatActivity.class));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        presenter.onBackPressed();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        presenter.onQueryTextSubmit(super.formatQuery(query));
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        presenter.onQueryTextSubmit(super.formatQuery(newText));
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public void onItemClickListener(UserDetails item) {
        presenter.onItemSelected(item);
    }
}