package com.firebasechat.screen;


import com.firebasechat.models.UserDetails;

public interface RecycleItemClickListener {

    void onItemClickListener(UserDetails item);
}