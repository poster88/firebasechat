package com.firebasechat.screen;


import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.firebasechat.R;
import com.firebasechat.utils.AppConstants;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;

import de.hdodenhof.circleimageview.CircleImageView;

public class BaseAdapter<T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T> {


    @Override
    public T onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(T holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    protected void loadImage(final CircleImageView image, final String imageUrl) {
        if (imageUrl == null) {
            image.setImageResource(R.drawable.ic_default_avatar_40);
        } else {
            Picasso.with(image.getContext()).load(imageUrl).into(image);
        }
    }

    @SuppressLint("SimpleDateFormat")
    protected String formatTime(long timestamp) {
        return new SimpleDateFormat(AppConstants.TIMESTAMP_FORMAT).format(timestamp);
    }
}