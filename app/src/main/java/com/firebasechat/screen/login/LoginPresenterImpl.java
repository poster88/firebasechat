package com.firebasechat.screen.login;


import com.firebasechat.R;
import com.firebasechat.repository.FirebaseChatRepository;
import com.firebasechat.utils.CommonUtils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.firebasechat.utils.AppConstants.ERROR_EMAIL_IS_BADLY_FORMATTED;
import static com.firebasechat.utils.AppConstants.ERROR_INVALID_PASSWORD;
import static com.firebasechat.utils.AppConstants.ERROR_NO_USER_IDENTIFIER;
import static com.firebasechat.utils.AppConstants.PASS_MIN_LENGTH;

public class LoginPresenterImpl implements LoginPresenter {

    public static final String TAG = LoginPresenterImpl.class.getSimpleName();

    private FirebaseChatRepository repository;
    private LoginView view;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public LoginPresenterImpl(FirebaseChatRepository repository, LoginView view) {
        this.repository = repository;
        this.view = view;
    }

    @Override
    public void init() {
        if (repository.getUserId() != null && view != null) {
            view.showUsersListScreen();
        }
    }

    @Override
    public void onLoginBtnClick(String email, String password, boolean isErrorEnabled) {
        if (!isErrorEnabled && validateFields(email, password)) {
            compositeDisposable.add(startAuth(email, password));
        }
    }

    private Disposable startAuth(String email, String password) {
        return repository.auth(email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    if (view != null) view.showProgress();
                })
                .doAfterTerminate(() -> {
                    if (view != null) view.hideProgress();
                })
                .subscribe(() -> {
                            if (view != null) view.showUsersListScreen();
                        },
                        throwable -> {
                            if (view != null) handleError(throwable.getMessage());
                        });
    }

    private void handleError(String error) {
        if (error.contains(ERROR_NO_USER_IDENTIFIER)) {
            view.showLoginError(R.string.error_no_user_identifier);
        } else if (error.contains(ERROR_EMAIL_IS_BADLY_FORMATTED)) {
            view.showLoginError(R.string.error_email_badly_formatted);
        } else if (error.contains(ERROR_INVALID_PASSWORD)) {
            view.showPasswordError(R.string.error_invalid_password);
        } else {
            view.showErrorMessage(error);
        }
    }

    @Override
    public void onDestroy() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
        view = null;
    }

    private boolean validateFields(String email, String password) {
        boolean isValid = true;
        if (email.isEmpty() && view != null) {
            view.showLoginError(R.string.error_empty_field);
            isValid = false;
        }
        if (!CommonUtils.isEmailValid(email) && view != null) {
            view.showLoginError(R.string.error_email_badly_formatted);
            isValid = false;
        }
        if (password.isEmpty() && view != null) {
            view.showPasswordError(R.string.error_empty_field);
            return false;
        }
        if (password.length() < PASS_MIN_LENGTH && view != null) {
            view.showPasswordError(R.string.error_invalid_length);
            isValid = false;
        }
        return isValid;
    }
}