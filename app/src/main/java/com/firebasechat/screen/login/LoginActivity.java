package com.firebasechat.screen.login;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.EditText;
import android.widget.TextView;

import com.firebasechat.screen.BaseActivity;
import com.firebasechat.screen.general.ChatDialogFragment;
import com.firebasechat.screen.general.ChatDialogView;
import com.firebasechat.screen.WatchedEditText;
import com.firebasechat.R;
import com.firebasechat.app.App;
import com.firebasechat.screen.registration.RegistrationActivity;
import com.firebasechat.screen.userslist.UsersListActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.VISIBLE;


public class LoginActivity extends BaseActivity implements LoginView {

    public static final String TAG = LoginActivity.class.getSimpleName();

    @BindView(R.id.activity_login_et_login) EditText etEmail;
    @BindView(R.id.activity_login_et_password) EditText etPassword;
    @BindView(R.id.activity_login_txt_error_login) TextView textLoginError;
    @BindView(R.id.activity_login_txt_error_password) TextView textPasswordError;

    private LoginPresenter presenter;
    private ChatDialogView chatDialogFragmentView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        startWatchForEditText();

        chatDialogFragmentView = ChatDialogFragment.view(getSupportFragmentManager(),
                getString(R.string.dialog_loading_message));
        presenter = new LoginPresenterImpl(App.getFirebaseChatRepository(), this);
        presenter.init();
    }

    private void startWatchForEditText() {
        new WatchedEditText().startWatchForEditText(etEmail, textLoginError);
        new WatchedEditText().startWatchForEditText(etPassword, textPasswordError);
    }

    @OnClick(R.id.activity_login_btn_login)
    public void onLoginBtnClick() {
        presenter.onLoginBtnClick(super.formatString(etEmail.getText().toString()),
                super.formatString(etPassword.getText().toString()),
                isErrorMessageEnable());
    }

    private boolean isErrorMessageEnable() {
        return super.isVisible(textLoginError) || super.isVisible(textPasswordError);
    }

    @OnClick(R.id.activity_login_txt_create_account)
    public void onCreateAccountBtnClick() {
        startActivity(new Intent(this, RegistrationActivity.class));
    }

    @Override
    public void showProgress() {
        chatDialogFragmentView.showProgress();
    }

    @Override
    public void hideProgress() {
        chatDialogFragmentView.hideProgress();
    }

    @Override
    public void showUsersListScreen() {
        startActivity(new Intent(this, UsersListActivity.class));
    }

    @Override
    public void showErrorMessage(String message) {
        super.showToast(this, message);
    }

    @Override
    public void showLoginError(int errorResId) {
        showEditTextError(etEmail, textLoginError, getString(errorResId));
    }

    @Override
    public void showPasswordError(int errorResId) {
        showEditTextError(etPassword, textPasswordError, getString(errorResId));
    }

    private void showEditTextError(EditText editText,TextView textView, String error) {
        editText.setBackgroundResource(R.drawable.et_round_error_bg);
        textView.setVisibility(VISIBLE);
        textView.setText(error);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }
}