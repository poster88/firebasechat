package com.firebasechat.screen.login;


public interface LoginView {

    void showProgress();

    void hideProgress();

    void showUsersListScreen();

    void showErrorMessage(String message);

    void showLoginError(int errorResId);

    void showPasswordError(int errorResId);
}