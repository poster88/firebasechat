package com.firebasechat.screen.login;


public interface LoginPresenter {

    void init();

    void onLoginBtnClick(String email, String password, boolean isErrorEnabled);

    void onDestroy();
}