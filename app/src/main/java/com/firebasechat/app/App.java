package com.firebasechat.app;


import android.app.Application;
import android.content.Context;
import com.firebasechat.repository.FirebaseChatRepository;
import com.firebasechat.repository.FirebaseChatRepositoryImpl;
import com.google.firebase.database.FirebaseDatabase;

public class App extends Application {

    private static FirebaseChatRepository firebaseChatRepository;
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        setPersistenceEnabled();
    }

    private void setPersistenceEnabled() {
        FirebaseDatabase.getInstance()
                .setPersistenceEnabled(true);
    }

    public static FirebaseChatRepository getFirebaseChatRepository() {
        if (firebaseChatRepository == null) {
            firebaseChatRepository = new FirebaseChatRepositoryImpl(context);
        }
        return firebaseChatRepository;
    }
}