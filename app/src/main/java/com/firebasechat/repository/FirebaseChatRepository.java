package com.firebasechat.repository;


import com.firebasechat.models.ChatItem;
import com.firebasechat.models.RegistrationData;
import com.firebasechat.models.User;
import com.firebasechat.models.UserDetails;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public interface FirebaseChatRepository {

    Completable registerUser(RegistrationData data);

    Observable<List<UserDetails>> getUsersDetails();

    Completable auth(String email, String password);

    Single<List<UserDetails>> getFilteredUsersDetails(String query);

    Single<User> getUserData();

    Observable<List<ChatItem>> initChat();

    User getReceiverData();

    Observable<Integer> addChatEventListener();

    Observable<String> addListEventListener();

    String getUserId();

    void signOut();

    void updateCurrentUserData(User data);

    void updateReceiverUserData(UserDetails data);

    void saveUsersDetailsData(List<UserDetails> usersDetails);

    void sendMessage(String msg);

    void removeChatEventListener();

    void clearData();

    void removeUserListEventListener();
}