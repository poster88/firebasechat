package com.firebasechat.repository;


import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.util.LruCache;

import com.firebasechat.models.ChatItem;
import com.firebasechat.models.Chats;
import com.firebasechat.models.Message;
import com.firebasechat.models.RegistrationData;
import com.firebasechat.models.User;

import com.firebasechat.models.UserDetails;
import com.firebasechat.screen.ChildValueListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.subjects.PublishSubject;

import static com.firebasechat.screen.chat.ChatAdapter.VIEW_TYPE_RECEIVER;
import static com.firebasechat.screen.chat.ChatAdapter.VIEW_TYPE_SENDER;
import static com.firebasechat.utils.AppConstants.KEY_NAME;
import static com.firebasechat.utils.AppConstants.KEY_USER_DETAILS_LIST;
import static com.firebasechat.utils.AppConstants.PATH_CHATS;
import static com.firebasechat.utils.AppConstants.PATH_MESSAGES;
import static com.firebasechat.utils.AppConstants.PATH_USERS;
import static com.firebasechat.utils.AppConstants.PATH_USERS_IMAGES;


public class FirebaseChatRepositoryImpl implements FirebaseChatRepository {

    public static final String TAG = FirebaseChatRepositoryImpl.class.getName();

    private Context context;
    private List<User> users = new ArrayList<>();
    private List<UserDetails> userDetailsList = new ArrayList<>();
    private List<ChatItem> chatItemList = new ArrayList<>();
    private LruCache<String, List<UserDetails>> usersDetailsListCache = new LruCache<>(2);
    private User currentUser;
    private UserDetails receiverUserDetails;
    private DatabaseReference chatReference;
    private PublishSubject<Integer> chatEventSubject = PublishSubject.create();
    private PublishSubject<String> listEventSubject = PublishSubject.create();
    private ChildValueListener chatEventListener = new ChildValueListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            ChatItem chatItem = getChatItem(dataSnapshot);
            if (!chatItemList.contains(chatItem)) {
                chatItemList.add(chatItem);
                chatEventSubject.onNext(chatItemList.size());
            }
        }
    };
    private ChildValueListener listEventListener = new ChildValueListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            String userId = dataSnapshot.getKey();
            for (UserDetails details: userDetailsList) {
                if (!details.getUser().getId().equals(userId)) {
                    listEventSubject.onNext(userId);
                    break;
                }
            }
        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {
            listEventSubject.onNext(dataSnapshot.getKey());
        }
    };

    public FirebaseChatRepositoryImpl(Context context) {
        this.context = context;
    }

    @Override
    public Completable registerUser(RegistrationData data) {
        return Single.create((SingleOnSubscribe<RegistrationData>) emitter ->
                FirebaseAuth.getInstance()
                        .createUserWithEmailAndPassword(data.getEmail(), data.getPassword())
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                emitter.onSuccess(data);
                            } else {
                                if (!emitter.isDisposed()) {
                                    emitter.onError(task.getException().fillInStackTrace());
                                }
                            }
                        }))
                .flatMap(this::initUserData)
                .flatMapCompletable(this::updateUserInDB);
    }

    private Completable updateUserInDB(User user) {
        return Completable.create(emitter -> FirebaseDatabase.getInstance()
                .getReference(PATH_USERS)
                .updateChildren(user.toMap())
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        emitter.onComplete();
                    } else {
                        if (!emitter.isDisposed()) {
                            emitter.onError(task.getException().fillInStackTrace());
                        }
                    }
                }));
    }

    private Single<User> initUserData(RegistrationData data) {
        return Single.create(emitter -> {
            Uri photoUri = data.getPhotoUri();
            User user = new User(getUserId(), data.getName(), data.getEmail());
            if (photoUri != null) {
                createUploadTask(photoUri).addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        String url = task.getResult().getDownloadUrl().toString();
                        user.setImageUrl(url);
                    }
                    emitter.onSuccess(user);
                });
            } else {
                emitter.onSuccess(user);
            }
        });
    }

    private Observable<List<User>> getUsersInfo() {
        return Observable.create(emitter -> FirebaseDatabase.getInstance()
                .getReference(PATH_USERS)
                .orderByChild(KEY_NAME)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        users.clear();
                        if (dataSnapshot.exists()) {
                            updateUsersList(dataSnapshot);
                        }
                        emitter.onNext(users);
                        emitter.onComplete();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (!emitter.isDisposed()) {
                            emitter.onError(databaseError.toException().fillInStackTrace());
                        }
                    }
                }));
    }

    private Observable<Map<String, String>> getChatDetails() {
        return Observable.create(emitter -> FirebaseDatabase.getInstance()
                .getReference(PATH_CHATS)
                .child(getUserId())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Map<String, String> chatDetails = new HashMap<>();
                        if (dataSnapshot.exists()) {
                            for (DataSnapshot data : dataSnapshot.getChildren()) {
                                chatDetails.put(data.getKey(), data.getValue(String.class));
                            }
                        }
                        emitter.onNext(chatDetails);
                        emitter.onComplete();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.d(TAG, databaseError.getDetails());
                        if (!emitter.isDisposed()) {
                            emitter.onError(databaseError.toException().fillInStackTrace());
                        }
                    }
                }));
    }

    @Override
    public Observable<List<UserDetails>> getUsersDetails() {
        return Observable.zip(getUsersInfo(), getChatDetails(), this::getUpdatedUserDetailsList);
    }

    @Override
    public Completable auth(String email, String password) {
        return Completable.create(emitter ->
                FirebaseAuth.getInstance()
                        .signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                emitter.onComplete();
                            } else {
                                if (!emitter.isDisposed()) {
                                    emitter.onError(task.getException().fillInStackTrace());
                                }
                            }
                        }));
    }

    @Override
    public Single<List<UserDetails>> getFilteredUsersDetails(String query) {
        return Observable.fromIterable(userDetailsList)
                .filter(userDetails -> {
                    String name = userDetails.getUser().getName().trim().toLowerCase();
                    return name.contains(query);
                })
                .toList()
                .map(filteredUserDetailsList -> {
                    boolean isEmpty = filteredUserDetailsList.isEmpty() && query.isEmpty();
                    return isEmpty ? userDetailsList : filteredUserDetailsList;
                });
    }

    @Override
    public Single<User> getUserData() {
        return Single.create(emitter -> FirebaseDatabase.getInstance()
                .getReference(PATH_USERS)
                .child(getUserId())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);
                        emitter.onSuccess(user);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (!emitter.isDisposed()) {
                            emitter.onError(databaseError.toException().fillInStackTrace());
                        }
                    }
                }));
    }

    @Override
    public void signOut() {
        FirebaseAuth.getInstance()
                .signOut();
    }

    @Override
    public void updateCurrentUserData(User data) {
        currentUser = data;
    }

    @Override
    public void updateReceiverUserData(UserDetails data) {
        receiverUserDetails = data;
    }

    @Override
    public void saveUsersDetailsData(List<UserDetails> usersDetails) {
        usersDetailsListCache.put(KEY_USER_DETAILS_LIST, usersDetails);
    }

    @Override
    public void sendMessage(String message) {
        chatReference.updateChildren(messageToMap(message, currentUser.getId()));
    }

    @Override
    public Observable<List<ChatItem>> initChat() {
        return Observable.just(receiverUserDetails)
                .flatMap(userDetails -> getChatItemList(userDetails.getChatId()));
    }

    @Override
    public User getReceiverData() {
        return receiverUserDetails.getUser();
    }

    @Override
    public Observable<Integer> addChatEventListener() {
        if (chatReference != null) {
            chatReference.addChildEventListener(chatEventListener);
        }
        return chatEventSubject;
    }

    @Override
    public void removeChatEventListener() {
        if (chatReference != null) {
            chatReference.removeEventListener(chatEventListener);
        }
        if (chatItemList.isEmpty()) {
            receiverUserDetails.setChatId(null);
            removeChatReferences(currentUser.getId(), receiverUserDetails.getUser().getId());
        }
    }

    @Override
    public void clearData() {
        chatItemList.clear();
    }

    private DatabaseReference userChatsReference;

    @Override
    public Observable<String> addListEventListener() {
        userChatsReference = FirebaseDatabase.getInstance()
                .getReference(PATH_CHATS)
                .child(getUserId());

        userChatsReference.addChildEventListener(listEventListener);
        return listEventSubject;
    }

    @Override
    public void removeUserListEventListener() {
        userChatsReference.removeEventListener(listEventListener);
    }

    @Override
    public String getUserId() {
        return FirebaseAuth.getInstance()
                .getUid();
    }

    private void removeChatReferences(String senderId, String receiverId) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference(PATH_CHATS);
        ref.child(senderId).child(receiverId).removeValue();
        ref.child(receiverId).child(senderId).removeValue();
        chatReference.removeValue();
    }

    private Observable<List<ChatItem>> getChatItemList(String chatId) {
        return Observable.create(emitter -> {
            if (chatId == null) {
                String newChatId = createChatWithUser(currentUser.getId(), receiverUserDetails.getUser().getId());
                receiverUserDetails.setChatId(newChatId);
            }
            chatReference = getChatReference(receiverUserDetails.getChatId());
            chatReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    chatItemList.clear();
                    if (dataSnapshot.exists()) {
                        addChatItemToList(dataSnapshot);
                    }
                    emitter.onNext(chatItemList);
                    emitter.onComplete();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    if (!emitter.isDisposed()) {
                        emitter.onError(databaseError.toException().fillInStackTrace());
                    }
                }
            });
        });
    }

    private List<UserDetails> getUpdatedUserDetailsList(List<User> users, Map<String, String> chatDetails) {
        userDetailsList.clear();
        for (User user : users) {
            UserDetails userDetails = new UserDetails();
            userDetails.setUser(user);
            boolean isChatEnabled = chatDetails.containsKey(user.getId());
            userDetails.setChatId(isChatEnabled ? chatDetails.get(user.getId()) : null);
            userDetailsList.add(userDetails);
        }
        return userDetailsList;
    }

    private void updateUsersList(DataSnapshot dataSnapshot) {
        String currentUserId = getUserId();
        for (DataSnapshot data : dataSnapshot.getChildren()) {
            User user = data.getValue(User.class);
            if (!currentUserId.equals(user.getId())) users.add(user);
        }
    }

    private void addChatItemToList(DataSnapshot dataSnapshot) {
        for (DataSnapshot data : dataSnapshot.getChildren()) {
            ChatItem chatItem = getChatItem(data);
            if (!chatItemList.contains(chatItem)) chatItemList.add(chatItem);
        }
    }

    private ChatItem getChatItem(DataSnapshot data) {
        Message msg = data.getValue(Message.class);
        boolean isCurrentUserMessage = currentUser.getId().equals(msg.getAuthorId());
        String imageUrl = isCurrentUserMessage ? currentUser.getImageUrl()
                : receiverUserDetails.getUser().getImageUrl();
        int viewType = isCurrentUserMessage ? VIEW_TYPE_SENDER
                : VIEW_TYPE_RECEIVER;
        return new ChatItem(imageUrl, viewType, msg);
    }

    private String createChatWithUser(String senderId, String receiverId) {
        String chatId = generateChatId();
        createChat(senderId, receiverId, chatId);
        createChat(receiverId, senderId, chatId);
        return chatId;
    }

    private UploadTask createUploadTask(Uri photoUri) {
        return FirebaseStorage.getInstance()
                .getReference(PATH_USERS_IMAGES)
                .child(getPathFromUri(photoUri))
                .putFile(photoUri);
    }

    private String getPathFromUri(Uri photoUri) {
        String path = photoUri.getLastPathSegment();
        path = path.substring(path.lastIndexOf(File.separator) + 1);
        return path;
    }

    private void createChat(String currentUserId, String receiverId, String chatId) {
        Chats chats = new Chats(receiverId, chatId);
        FirebaseDatabase.getInstance()
                .getReference(PATH_CHATS)
                .child(currentUserId)
                .updateChildren(chats.toMap());
    }

    private String generateChatId() {
        return FirebaseDatabase.getInstance()
                .getReference()
                .push()
                .getKey();
    }

    private DatabaseReference getChatReference(String chatId) {
        return FirebaseDatabase.getInstance()
                .getReference(PATH_MESSAGES)
                .child(chatId);
    }

    private Map<String, Object> messageToMap(String message, String userId) {
        long timeStamp = (new Timestamp(System.currentTimeMillis())).getTime();
        Message msg = new Message(userId, message, timeStamp);
        Map<String, Object> map = new HashMap<>();
        map.put(generateKey(), msg);
        return map;
    }

    private String generateKey() {
        return FirebaseDatabase.getInstance()
                .getReference()
                .push()
                .getKey();
    }
}