# Firebase chat #

* Приложение позволяет зарегистрированным пользователям переписываться между собой, просматривать историю переписки.
* В проекте использовал один из архитектурных паттернов - MVP. 
* Для построения UI компонентов использовал рекомендации из Material Design.
* Доступная ориентация экрана: portrait.
* Min SDK API level l9: Android 4.4. 
 
### Uses libraries ###

* *RxJava 2/RxAndroid* – используется для работы с многопоточностю.
* *FirebaseAuth* – используеться для аутентификации пользователя
* *FirebaseDatabase* – используеться как удаленный репозиторий данных.
* *FirebaseStorage* – используется для удаленного сохранения файлов.
* *ButterKnife* – позволяет найти и автоматически отобразить соответствующее представление в макете.
* *Picasso* – используеться для загрузки изображений.

### Links ###

Скачать **APK** файл можна [здесь](https://drive.google.com/file/d/1Ey1sSahmAjYOSo4_cg_ik4OZp7uvP07V/view?usp=sharing)

### Screens ###

![Alt text](https://bytebucket.org/poster88/firebasechat/raw/3f18c441fff209360c2ef850dcdecebc5448dfb3/README/FirebaseChatCollages.png)